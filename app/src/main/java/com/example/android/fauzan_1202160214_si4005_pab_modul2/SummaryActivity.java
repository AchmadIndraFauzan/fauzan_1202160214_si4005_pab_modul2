package com.example.android.fauzan_1202160214_si4005_pab_modul2;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;

public class SummaryActivity extends AppCompatActivity {
    public static final String TUJUAN = "tujuan";
    public static final String TANGGAL_BERANGKAT = "tanggal";
    public static final String JAM_BERANGKAT = "jam";
    public static final String TANGGAL_PULANG = "tanggal2";
    public static final String JAM_PULANG = "jam2";
    public static final String JUMLAH_TIKET = "saldo";
    public static final String SALDO_AKHIR = "jumlah";
    public static final String HARGA_TOTAL = "total";

    //deklarasi
    TextView lokasiTv;
    TextView tanggal_berangkatTv;
    TextView jam_berangkatTv;
    TextView tanggal_pulangTv;
    TextView jam_pulangTv;
    TextView hargaTv;
    TextView jumlahTv;
    TextView SubJudulTP;
    Button konfirmasiBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        Log.d(SummaryActivity.class.getSimpleName(), "onCreate");

        lokasiTv = findViewById(R.id.lokasiTv);
        tanggal_berangkatTv = findViewById(R.id.tanggal_berangkatTv);
        jam_berangkatTv = findViewById(R.id.jam_berangkatTv);
        tanggal_pulangTv = findViewById(R.id.tanggal_pulangTv);
        jam_pulangTv = findViewById(R.id.jam_pulangTv);
        SubJudulTP = findViewById(R.id.SubJudulTP);
        jumlahTv = findViewById(R.id.jumlahTv);
        hargaTv = findViewById(R.id.hargaTv);
        konfirmasiBtn = findViewById(R.id.konfirmasiBtn);

        lokasiTv.setText(getIntent().getStringExtra(TUJUAN));

        tanggal_berangkatTv.setText(getIntent().getStringExtra(TANGGAL_BERANGKAT));
        jam_berangkatTv.setText(getIntent().getStringExtra(JAM_BERANGKAT));


        if(getIntent().hasExtra(TANGGAL_PULANG)){
            tanggal_pulangTv.setText(getIntent().getStringExtra(TANGGAL_PULANG));
            jam_pulangTv.setText(getIntent().getStringExtra(JAM_PULANG));
        }else{
            SubJudulTP.setVisibility(View.GONE);
            tanggal_pulangTv.setVisibility(View.GONE);
            jam_pulangTv.setVisibility(View.GONE);
        }


        jumlahTv.setText(getIntent().getStringExtra(JUMLAH_TIKET));
        hargaTv.setText(getIntent().getStringExtra(HARGA_TOTAL));

        konfirmasiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder =  new AlertDialog.Builder(SummaryActivity.this);
                builder.setMessage("Apakah anda yakin?")
                        .setNegativeButton("Cancel", null)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    MainActivity main = new MainActivity();
                                    int tagihan = Integer.parseInt(hargaTv.getText().toString());
                                    String saldoAkhir = getIntent().getStringExtra(SALDO_AKHIR);
                                    Intent current = new Intent(SummaryActivity.this, MainActivity.class);

                                    current.putExtra(MainActivity.CURRENT_SALDO, String.valueOf(Integer.valueOf(saldoAkhir) - tagihan));
                                    finish();
                                    startActivity(current);
                                }
                            });
                builder.show();
            }
        });


    }
    public void onStart(){
        super.onStart();
        Log.d(SummaryActivity.class.getSimpleName(), "onStart");
    }
    public void onPause(){
        super.onPause();
        Log.d(SummaryActivity.class.getSimpleName(), "onPause");

    }
    public void onRestart(){
        super.onRestart();
        Log.d(SummaryActivity.class.getSimpleName(), "onRestart");

    }
    public void onResume(){
        super.onResume();
        Log.d(SummaryActivity.class.getSimpleName(), "onResume");

    }
    public void onStop(){
        super.onStop();
        Log.d(SummaryActivity.class.getSimpleName(), "onStop");
    }
    public void onDestroy(){
        super.onDestroy();
        Log.d(SummaryActivity.class.getSimpleName(), "onDestroy");
    }
}
