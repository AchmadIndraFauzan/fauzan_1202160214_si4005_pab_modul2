package com.example.android.fauzan_1202160214_si4005_pab_modul2;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    TextView topUpBtn;
    TextView saldo;
    Spinner tujuan;
    Switch pulangpergi;
    TextView datePicker;
    TextView timePicker;
    TextView datePicker2;
    TextView timePicker2;
    TextView thxTv;
    EditText jumlah;
    Button buyBtn;
    int hargaTiket;
    String Datatujuan;
    DatePickerDialog tanggalPulang, tanggalBerangkat;
    int pp = 1; //tarif pulang pergi
    private static final String[] dataTujuan = {"Jakarta (Rp. 85.000)", "Cirebon (Rp.150.000)", "Bekasi (Rp.70.000)"};
    public static final String CURRENT_SALDO = "SALDO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Log.d(MainActivity.class.getSimpleName(), "onCreate");

        //saldo
        saldo = findViewById(R.id.saldo);
        thxTv = findViewById(R.id.thxTv);
        if(getIntent().hasExtra(CURRENT_SALDO)){
            int hasil = Integer.parseInt(getIntent().getStringExtra(CURRENT_SALDO));
            saldo.setText(hasil + "");
            thxTv.setText("Terima kasih ! Semoga selamat sampai tujuan");
        }

        //top up saldo

        topUpBtn = findViewById(R.id.topUpBtn);
        topUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder topUp = new AlertDialog.Builder(MainActivity.this);
                final View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.activity_top_up, null);

                topUp.setNegativeButton("CANCEL", null)
                        .setPositiveButton("TAMBAH SALDO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                TextView jumlahSaldo;
                                jumlahSaldo = view.findViewById(R.id.jumlahSaldo);
                                int realSaldo = Integer.parseInt(saldo.getText().toString());
                                int Datasaldo = Integer.parseInt(jumlahSaldo.getText().toString());
                                int hasil = realSaldo + Datasaldo;
                                saldo.setText(String.valueOf(hasil));
                            }
                        });
                topUp.setView(view);
                topUp.show();
            }
        });

        //pilih tujuan
        tujuan = findViewById(R.id.tujuan);
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_spinner_item,dataTujuan);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tujuan.setAdapter(adapter);
        tujuan.setOnItemSelectedListener(this);

        //date picker
        datePicker = findViewById(R.id.datePicker);
        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();

                DatePickerDialog tanggalBerangkat = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                        datePicker.setText(dateFormatter.format(newDate.getTime()));
                    }

                },calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                tanggalBerangkat.show();


            }
        });

        //time picker
        timePicker = findViewById(R.id.timePicker);
        timePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);
                TimePickerDialog jamBerangkat;
                jamBerangkat = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker time, int selectedHour, int selectedMinute) {
                        timePicker.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);
                jamBerangkat.setTitle("Select Time");
                jamBerangkat.show();

            }
        });

        //pulang pergi
        pulangpergi = findViewById(R.id.pulangpergi);
        datePicker2 = findViewById(R.id.datePicker2);
        timePicker2 = findViewById(R.id.timePicker2);
        pulangpergi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(pulangpergi.isChecked()){
                    datePicker2.setVisibility(View.VISIBLE);
                    timePicker2.setVisibility(View.VISIBLE);

                    datePicker2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar newCalendar = Calendar.getInstance();

                            DatePickerDialog tanggalPulang = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                    Calendar newDate = Calendar.getInstance();
                                    newDate.set(year, monthOfYear, dayOfMonth);
                                    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                                    datePicker2.setText(dateFormatter.format(newDate.getTime()));
                                }

                            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                            tanggalPulang.show();


                        }
                    });

                    timePicker2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar mcurrentTime = Calendar.getInstance();
                            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                            int minute = mcurrentTime.get(Calendar.MINUTE);
                            TimePickerDialog jamPulang;
                            jamPulang = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker time, int selectedHour, int selectedMinute) {
                                    timePicker2.setText(selectedHour + ":" + selectedMinute);
                                }
                            }, hour, minute, true);
                            jamPulang.setTitle("Select Time");
                            jamPulang.show();

                        }
                    });

                    pp = 2;


                }else{
                    datePicker2.setVisibility(View.GONE);
                    timePicker2.setVisibility(View.GONE);
                    pp = 1;
                }
            }
        });

        buyBtn = findViewById(R.id.buyBtn);
        jumlah = findViewById(R.id.jumlah);
        buyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (jumlah.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, "MASUKKAN JUMLAH TIKET",
                            Toast.LENGTH_LONG).show();
                }else{
                    int saldoData = Integer.parseInt(saldo.getText().toString());
                    int jumlahData = Integer.parseInt(jumlah.getText().toString());
                    int totalTagihan = hargaTiket * pp * jumlahData;
                    if(saldoData < totalTagihan){
                        Toast.makeText(MainActivity.this, "SALDO TIDAK CUKUP",
                                Toast.LENGTH_LONG).show();
                    }else{
                        Intent summary = new Intent(MainActivity.this, SummaryActivity.class);
                        summary.putExtra(SummaryActivity.SALDO_AKHIR, saldo.getText().toString());
                        summary.putExtra(SummaryActivity.TUJUAN, Datatujuan);

                        String date = datePicker.getText().toString();
                        String time = timePicker.getText().toString();
                        summary.putExtra(SummaryActivity.TANGGAL_BERANGKAT, date);
                        summary.putExtra(SummaryActivity.JAM_BERANGKAT, time);
                        if(pp != 1){
                            String datePP = datePicker2.getText().toString();
                            String timePP = timePicker2.getText().toString();
                            summary.putExtra(SummaryActivity.TANGGAL_PULANG, datePP);
                            summary.putExtra(SummaryActivity.JAM_PULANG, timePP);
                        }
                        summary.putExtra(SummaryActivity.JUMLAH_TIKET, jumlah.getText().toString());

                        int total = hargaTiket * pp * jumlahData;
                        summary.putExtra(SummaryActivity.HARGA_TOTAL, String.valueOf(total));
                        startActivity(summary);

                    }
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(position == 0){
            Datatujuan = "Jakarta";
            hargaTiket = 85000;
        }else if(position == 1){
            Datatujuan = "Cirebon";
            hargaTiket = 150000;
        }else{
            Datatujuan = "Bekasi";
            hargaTiket = 70000;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onStart(){
        super.onStart();
        Log.d(MainActivity.class.getSimpleName(), "onStart");
    }
    public void onPause(){
        super.onPause();
        Log.d(MainActivity.class.getSimpleName(), "onPause");

    }
    public void onRestart(){
        super.onRestart();
        Log.d(MainActivity.class.getSimpleName(), "onRestart");

    }
    public void onResume(){
        super.onResume();
        Log.d(MainActivity.class.getSimpleName(), "onResume");

    }
    public void onStop(){
        super.onStop();
        Log.d(MainActivity.class.getSimpleName(), "onStop");
    }
    public void onDestroy(){
        super.onDestroy();
        Log.d(MainActivity.class.getSimpleName(), "onDestroy");
    }

}
